package com.fivs.electionresult.ui

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionadminapp.data.model.PartyModel
import com.fivs.electionadminapp.data.model.StateModel
import com.fivs.electionresult.R
import com.fivs.electionresult.adapter.AdapterCandidateResultList
import com.fivs.electionresult.adapter.AdapterDialogConstituencyList
import com.fivs.electionresult.adapter.AdapterDialogStateList
import com.fivs.electionresult.callback.CandidateClickCallback
import com.fivs.electionresult.callback.ConstituencyClickCallback
import com.fivs.electionresult.callback.StateClickCallback
import com.fivs.electionresult.model.CandidateModel
import com.fivs.electionresult.utils.Constants
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : BaseActivity(), CandidateClickCallback {
    private var mDataList: ArrayList<CandidateModel> = arrayListOf()
    private var loading = false
    private var mStateDataList: java.util.ArrayList<StateModel> = arrayListOf()
    private var mSelectedState: StateModel = StateModel()
    private var mConstituencyDataList: java.util.ArrayList<ConstituencyModel> = arrayListOf()
    private var mSelectedConstituency: ConstituencyModel = ConstituencyModel()
    private lateinit var mAdapter: AdapterCandidateResultList

    private var mPartyDataList: ArrayList<PartyModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setScreenTitle(R.string.result)
        disableBack()
        initializeComponent()
        setClick()

        if (isNetworkAvailable()) {
            getElectionTime()
        } else {
            showMessage(this, getString(R.string.no_network))
        }

        initializeGraph()
        initializeRain()
    }

    private fun initializeComponent() {
        rvList.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterCandidateResultList(this, this, mDataList)
        rvList.adapter = mAdapter

    }

    fun setClick() {


        llState.setOnClickListener {
            if (mStateDataList.isNotEmpty())
                pickState()
        }
        llConstituency.setOnClickListener {
            if (mConstituencyDataList.isNotEmpty()) {
                pickConstituency()
            }
        }
        btnCheck.setOnClickListener {
            if (isNetworkAvailable()) {
                getElectionTime()
            } else {
                showMessage(this, getString(R.string.no_network))
            }
        }

    }

    fun getElectionTime() {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val calendar = Calendar.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_TIME)
        val query = ref
            .document(Constants.ELECTION_TIME_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_START_TIME)).toString() != "null") {
                    val parser =
                        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyy", Locale.getDefault())


                    val result =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_RESULT_TIME).toString())
                    if (result!!.before(calendar.time)) {
                        rlContent.visibility = View.VISIBLE
                        btnCheck.visibility = View.GONE
                        getStates()
                    } else {
                        rlContent.visibility = View.GONE
                        btnCheck.visibility = View.VISIBLE
                        showMessage(this, getString(R.string.result_has_not_reached))
                    }

                } else {
                    rlContent.visibility = View.GONE
                    btnCheck.visibility = View.VISIBLE
                    showMessage(this, getString(R.string.result_has_not_reached))
                }

                dismissProgressDialogPopup()

            }
            .addOnFailureListener {
                dismissProgressDialogPopup()
                rlContent.visibility = View.GONE
                btnCheck.visibility = View.VISIBLE
                showMessage(this, getString(R.string.result_has_not_reached))
            }
    }

    private fun getStates() {


        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: java.util.ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)


                }
                mStateDataList.addAll(mList)
                if (mStateDataList.isEmpty()) {
                    dismissProgressDialogPopup()
                    tvStateName.text = getString(R.string.no_state_available)
                    tvStateChange.visibility = View.GONE
                } else {
                    if (mSelectedState.id == "") {
                        mSelectedState = mStateDataList[0]
                        tvStateName.text = mSelectedState.name
                        mDataList.clear()

                    }
                    getParty(mSelectedState.id)
                    tvStateChange.visibility = View.VISIBLE
                }

            }
            .addOnFailureListener {
                dismissProgressDialogPopup()
            }
            .addOnCompleteListener {
                loading = false
            }

    }


    private fun getConstituency(stateID: String) {

        showProgressDialog(getString(R.string.loading))
        loading = true
        mConstituencyDataList.clear()
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<ConstituencyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    val mCategoryData =
                        ConstituencyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.CONSTITUENCY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()

                    mCategoryData.cast_vote =
                        (document.data?.get(Constants.CONSTITUENCY_CAST_VOTE)).toString().toInt()
                    mCategoryData.voter_count =
                        (document.data?.get(Constants.CONSTITUENCY_VOTER_COUNT)).toString().toInt()
                    mCategoryData.won_candidate =
                        (document.data?.get(Constants.CONSTITUENCY_WON_CANDIDATE)).toString()


                    mList.add(mCategoryData)


                }
                mConstituencyDataList.addAll(mList)
                if (mConstituencyDataList.isEmpty()) {
                    dismissProgressDialogPopup()
                    tvConstituencyName.text = getString(R.string.no_constituency_available)
                    tvConstituencyChange.visibility = View.GONE
                } else {
                    mSelectedConstituency = mConstituencyDataList[0]
                    tvConstituencyName.text = mSelectedConstituency.name
                    getCandidates(mSelectedConstituency.id)
                    tvConstituencyChange.visibility = View.VISIBLE
                }

            }
            .addOnFailureListener {
                dismissProgressDialogPopup()
            }
            .addOnCompleteListener {
                loading = false

            }

    }

    private fun getParty(stateId: String) {


        loading = true
        showProgressDialog(getString(R.string.loading))
        mPartyDataList.clear()
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_PARTY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateId)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: java.util.ArrayList<PartyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    val mCategoryData =
                        PartyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.PARTY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.PARTY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mCategoryData.isRegional =
                        (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                    mList.add(mCategoryData)


                }
                mPartyDataList.addAll(mList)

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
                mConstituencyDataList.clear()
                getConstituency(mSelectedState.id)
            }

    }

    private fun getCandidates(constituencyID: String) {

        if (mPartyDataList.isNotEmpty()) {
            loading = true
            mDataList.clear()
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_CANDIDATE)
            val query = ref
                .whereEqualTo(Constants.CONSTITUENCY_ID, constituencyID)
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)

            query.get()
                .addOnSuccessListener { fetchall_task ->

                    val mList: ArrayList<CandidateModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        val mCategoryData =
                            CandidateModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.CANDIDATE_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.CANDIDATE_NAME)).toString()
                        mCategoryData.logo =
                            (document.data?.get(Constants.LOGO)).toString()
                        mCategoryData.state = mSelectedState.name
                        mCategoryData.stateid =
                            (document.data?.get(Constants.STATE_ID)).toString()
                        mCategoryData.constituency = mSelectedConstituency.name
                        mCategoryData.constituencyid =
                            (document.data?.get(Constants.CONSTITUENCY_ID)).toString()
                        mCategoryData.partyid =
                            (document.data?.get(Constants.PARTY_ID)).toString()
                        if ((document.data?.get(Constants.CANDIDATE_VOTE)).toString() != "null") {
                            mCategoryData.vote =
                                (document.data?.get(Constants.CANDIDATE_VOTE)).toString().toInt()
                        } else {
                            mCategoryData.vote = 0
                        }
                        var partyname = ""
                        if (mPartyDataList.filter { it.id == mCategoryData.partyid }.isNotEmpty()) {
                            partyname =
                                mPartyDataList.filter { it.id == mCategoryData.partyid }.single()
                                    .name
                        }
                        mCategoryData.party = partyname
                        var partylogo = ""
                        if (mPartyDataList.filter { it.id == mCategoryData.partyid }.isNotEmpty()) {
                            partylogo =
                                mPartyDataList.filter { it.id == mCategoryData.partyid }.single()
                                    .image
                        }

                        mCategoryData.partyimage = partylogo
                        mList.add(mCategoryData)


                    }
                    mDataList.clear()
                    mDataList.addAll(mList)

                    if (mDataList.isEmpty()) {
                        tvNodata.visibility = View.VISIBLE
                        nsvData.visibility = View.GONE
                    } else {
                        tvNodata.visibility = View.GONE
                        nsvData.visibility = View.VISIBLE

                        mDataList.sortByDescending { it.vote }
                        mAdapter.notifyDataSetChanged()


                        var noWinner = false

                        if (mDataList.filter { it.vote == 0 }.size == mDataList.size) {
                            noWinner = true
                            llChart.visibility = View.GONE
                        } else {
                            val maxVote = mDataList.maxBy { it.vote }!!.vote

                            val tempList = mDataList.filter { it.vote == maxVote }

                            if (tempList.isNotEmpty() && tempList.size > 1) {
                                if (tempList.size == 2 && tempList.filter { it.name == "NOTA" }.isEmpty()) {
                                    noWinner = true
                                }
                            }
                            llChart.visibility = View.VISIBLE
                            setPolling(
                                mSelectedConstituency.cast_vote,
                                mSelectedConstituency.voter_count
                            )
                        }


                        if (noWinner) {
                            group_emoji_container.visibility = View.GONE
                            tvWinnerCannot.visibility = View.VISIBLE

                        } else {
                            group_emoji_container.visibility = View.VISIBLE
                            tvWinnerCannot.visibility = View.GONE
                            setWinner()
                        }

                        setCandidateVote()

                    }

                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        }
    }

    private fun pickState() {
        val infoDialog: Dialog? = createCustomDialog(
            this@MainActivity,
            R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogStateList(this, object :
            StateClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedState = mStateDataList[position]
                tvStateName.text = mSelectedState.name
                mDataList.clear()
                mAdapter.notifyDataSetChanged()
                mPartyDataList.clear()
                getParty(mSelectedState.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mStateDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun pickConstituency() {
        val infoDialog: Dialog? = createCustomDialog(
            this@MainActivity,
            R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogConstituencyList(this, object :
            ConstituencyClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedConstituency = mConstituencyDataList[position]
                tvConstituencyName.text = mSelectedConstituency.name
                mDataList.clear()
                mAdapter.notifyDataSetChanged()
                getCandidates(mSelectedConstituency.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mConstituencyDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onItemClick(position: Int) {

    }

    override fun onImageClick(position: Int) {
    }

    override fun onItemRemoveClick(position: Int) {
    }

    private fun setWinner() {
        var winnerData = mDataList[0]
        if (winnerData.name == "NOTA") {
            if (mDataList.size > 1) {

                val tempList = mDataList.filter { it.vote == mDataList[1].vote }
                if (tempList.size > 1) {
                    group_emoji_container.visibility = View.GONE
                    tvWinnerCannot.visibility = View.VISIBLE
                    return
                }
                winnerData = mDataList[1]
                var runnerData = CandidateModel()
                if (mDataList.size > 2) {
                    runnerData = mDataList[2]
                    if (runnerData.name == "NOTA") {
                        if (mDataList.size > 3) {
                            runnerData = mDataList[3]
                        }

                    }
                }
                var requestOptions1 = RequestOptions()
                requestOptions1 =
                    requestOptions1
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)

                Glide.with(this)
                    .load(winnerData.image)
                    .apply(requestOptions1)
                    .into(ivWinner)
                tvWinner.text = winnerData.name
                tvWinnerVote.text = winnerData.vote.toString()

                val displayString = StringBuilder()
                displayString.append(getString(R.string.lead))
                displayString.append((winnerData.vote - runnerData.vote).toString())
                tvWinnerLead.text = displayString.toString()

                group_emoji_container.startDropping()
            } else {
                group_emoji_container.visibility = View.GONE
                tvWinnerCannot.visibility = View.VISIBLE
            }
        } else {
            var requestOptions1 = RequestOptions()
            requestOptions1 =
                requestOptions1
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)

            Glide.with(this)
                .load(winnerData!!.image)
                .apply(requestOptions1)
                .into(ivWinner)
            tvWinner.text = winnerData!!.name
            tvWinnerVote.text = winnerData!!.vote.toString()

            if (mDataList.size > 2) {
                var runnerData = mDataList[1]
                if (runnerData.name == "NOTA") {
                    runnerData = mDataList[2]

                }
                val displayString = StringBuilder()
                displayString.append(getString(R.string.lead))
                displayString.append((winnerData.vote - runnerData.vote).toString())
                tvWinnerLead.text = displayString.toString()
            }

            group_emoji_container.startDropping()
        }

    }

    private fun setPolling(done: Int, total: Int) {
        val entries: ArrayList<PieEntry> = ArrayList()

        entries.add(PieEntry(done.toFloat(), "DONE"))
        entries.add(PieEntry((total - done).toFloat(), "PENDING"))

        val dataSet = PieDataSet(entries, "Election Results")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors: ArrayList<Int> = ArrayList()
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartPolling))
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLUE)
        // data.setValueTypeface(tfLight)
        chartPolling.data = data
        // undo all highlights
        chartPolling.highlightValues(null)
        chartPolling.invalidate()
    }

    private fun setCandidateVote() {
        val entries: ArrayList<PieEntry> = ArrayList()


        for (data in mDataList) {
            entries.add(PieEntry(data.vote.toFloat(), data.name + "(" + data.party + ")"))
        }
        val dataSet = PieDataSet(entries, "Election Results")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors: ArrayList<Int> = ArrayList()
        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)
        for (c in ColorTemplate.MATERIAL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartCandidateVote))
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLUE)
        // data.setValueTypeface(tfLight)
        chartCandidateVote.data = data
        // undo all highlights
        chartCandidateVote.highlightValues(null)
        chartCandidateVote.invalidate()
    }

    private fun initializeRain() {
        group_emoji_container.addEmoji(R.drawable.ash1)
        group_emoji_container.addEmoji(R.drawable.ash2)
        group_emoji_container.addEmoji(R.drawable.ash3)
        group_emoji_container.addEmoji(R.drawable.ash4)
        group_emoji_container.addEmoji(R.drawable.ash5)
        group_emoji_container.addEmoji(R.drawable.ash6)
        group_emoji_container.addEmoji(R.drawable.ash7)
        group_emoji_container.addEmoji(R.drawable.ash8)
        group_emoji_container.addEmoji(R.drawable.ash9)
        group_emoji_container.addEmoji(R.drawable.ash10)
        group_emoji_container.setPer(10)
        group_emoji_container.setDuration(2500)
        group_emoji_container.setDropDuration(1200)
        group_emoji_container.setDropFrequency(300)
    }

    private fun initializeGraph() {
        chartPolling.setUsePercentValues(true)
        chartPolling.description.isEnabled = false
        chartPolling.setExtraOffsets(5f, 10f, 5f, 5f)
        chartPolling.dragDecelerationFrictionCoef = 0.95f
        chartPolling.centerText = ""
        chartPolling.isDrawHoleEnabled = false
        chartPolling.setHoleColor(Color.WHITE)
        chartPolling.setTransparentCircleColor(Color.WHITE)
        chartPolling.setTransparentCircleAlpha(110)
        chartPolling.holeRadius = 58f
        chartPolling.transparentCircleRadius = 61f
        chartPolling.setDrawCenterText(true)
        chartPolling.rotationAngle = 0f
        chartPolling.isRotationEnabled = true
        chartPolling.isHighlightPerTapEnabled = true
        chartPolling.animateY(2000, Easing.EaseInOutQuad)
        /* val l = chartPolling.legend
         l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
         l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
         l.orientation = Legend.LegendOrientation.VERTICAL
         l.setDrawInside(false)
         l.xEntrySpace = 7f
         l.yEntrySpace = 0f
         l.yOffset = 0f*/
        chartPolling.setEntryLabelColor(Color.BLUE)
        chartPolling.setEntryLabelTextSize(10f)





        chartCandidateVote.setUsePercentValues(true)
        chartCandidateVote.description.isEnabled = false
        chartCandidateVote.setExtraOffsets(5f, 10f, 5f, 5f)
        chartCandidateVote.dragDecelerationFrictionCoef = 0.95f
        chartCandidateVote.centerText = ""
        chartCandidateVote.isDrawHoleEnabled = true
        chartCandidateVote.setHoleColor(Color.WHITE)
        chartCandidateVote.setTransparentCircleColor(Color.WHITE)
        chartCandidateVote.setTransparentCircleAlpha(110)
        chartCandidateVote.holeRadius = 40f
        chartCandidateVote.transparentCircleRadius = 61f
        chartCandidateVote.setDrawCenterText(true)
        chartCandidateVote.rotationAngle = 0f
        chartCandidateVote.isRotationEnabled = true
        chartCandidateVote.isHighlightPerTapEnabled = true
        chartCandidateVote.animateY(2000, Easing.EaseInOutQuad)
        /*val lcanditateVote = chartCandidateVote.legend
        lcanditateVote.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        lcanditateVote.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        lcanditateVote.orientation = Legend.LegendOrientation.VERTICAL
        lcanditateVote.setDrawInside(false)
        lcanditateVote.xEntrySpace = 7f
        lcanditateVote.yEntrySpace = 0f
        lcanditateVote.yOffset = 0f*/
        chartCandidateVote.setEntryLabelColor(Color.BLUE)
        chartCandidateVote.setEntryLabelTextSize(10f)

    }


}