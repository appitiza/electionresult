package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class StateModel:Serializable {
    var id : String = ""
    var image : String = ""
    var name : String = ""
    var cast_vote : Int = 0
    var voter_count : Int = 0
    var won_party : String = ""

}