package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class ConstituencyModel:Serializable {
    var id : String = ""
    var image : String = ""
    var name : String = ""
    var isInactive : String = "0"
    var cast_vote : Int = 0
    var voter_count : Int = 0
    var won_candidate : String = ""
    var won_lead : Int = 0
    var state : String = ""
    var stateid : String = ""

}