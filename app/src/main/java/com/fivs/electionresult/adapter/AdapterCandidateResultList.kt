package com.fivs.electionresult.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionresult.callback.CandidateClickCallback
import com.fivs.electionresult.model.CandidateModel
import com.fivs.electionresult.R
import kotlinx.android.synthetic.main.item_candidate_result_list.view.*

class AdapterCandidateResultList(
    var context: Context,
    private var callback: CandidateClickCallback,
    var mList: ArrayList<CandidateModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterCandidateResultList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_candidate_result_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: CandidateClickCallback,
            data: CandidateModel
        ) {


            var requestOptions1 = RequestOptions()
            requestOptions1 =
                requestOptions1.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)

            Glide.with(context)
                .load(data.image)
                .apply(requestOptions1)
                .into(itemView.ivCandidateImage)


            var requestOptions2 = RequestOptions()
            requestOptions2 =
                requestOptions2.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)

            if (data.party == "other") {
                Glide.with(context)
                    .load(data.logo)
                    .apply(requestOptions2)
                    .into(itemView.ivCandidateLogo)
            } else {
                Glide.with(context)
                    .load(data.partyimage)
                    .apply(requestOptions2)
                    .into(itemView.ivCandidateLogo)
            }


            itemView.tvName.text = data.name
            itemView.tvResult.text = data.vote.toString()
            itemView.tvConstituency.text = data.constituency
            itemView.tvState.text = data.state
            itemView.tvParty.text = data.party
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }
            itemView.ivCandidateImage.setOnClickListener {

                callback.onImageClick(position = adapterPosition)

            }

        }
    }

}